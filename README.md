# rust-dylib-ination

dylib-inate your Rust!

## Building

```shell
$ (cd project-a && cargo build --release)
$ cd project-b
project-b/ $ cargo build --release
project-b/ $ export LD_LIBRARY_PATH=$(pwd)/../project-a/target/release
project-b/ $ target/release/project_b
Hello, world!
```

